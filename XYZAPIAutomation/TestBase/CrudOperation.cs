﻿using System;


namespace XYZAPIAutomation.TestBase
{
    public class CrudOperation
    {
        public static void HttpGet()
        {
            Console.WriteLine("Perfoming GET call");
        }

        public static void HttpPost()
        {
            Console.WriteLine("Perfoming Post call");
        }

        public static void HttpPut()
        {
            Console.WriteLine("Perfoming Put call");
        }

        public static void HttpDelete()
        {
            Console.WriteLine("Perfoming Delete call");
        }

        public static void NewMethod()
        {
            Console.WriteLine("Adding a new method");
        }
    }
}
